﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models.Entities;
using Models;
namespace LiveEcoMap.Data
{
    public class ApplicationManager
    {
        #region Singleton

        private static ApplicationManager instance;
        public static ApplicationManager GetInstance()
        {
            if (instance == null) instance = new ApplicationManager();
            return instance;
        }

        #endregion

        public string connString = "";
        public ApplicationDbContext dbContext = new ApplicationDbContext(new Microsoft.EntityFrameworkCore.DbContextOptions<ApplicationDbContext> { });
    }
}

