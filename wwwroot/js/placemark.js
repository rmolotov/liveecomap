
ymaps.ready(init);

function init() {

    //Определение начальных параметров карты
    var myMap = new ymaps.Map("map", {
        center: [54.3282, 48.3866],
        zoom: 10
    }, {
        // Зададим ограниченную область прямоугольником,
        // примерно описывающим Санкт-Петербург.
        restrictMapArea: [
            [54.53, 47.9],
            [54.14, 48.81]
        ]
    });

    //Добавляем элементы управления
    myMap.controls
        .add('zoomControl')
        .add('typeSelector')
        .add('mapTools');


    //Отслеживаем событие клик левой кнопкой мыши на карте
    myMap.events.add('click', function (e) {
        if (!myMap.balloon.isOpen()) {
            var coords = e.get('coordPosition');
            myMap.balloon.open(coords, {
                contentBody: '<div id="menu">\
                             <div id="menu_list">\
                                <label>Название:</label> <input type="text" class="input-medium" name="icon_text" /><br />\
								 <div class="control-group">\
								    <div class="input-prepend">\
								        <select name="image" id="marker-type-selector" class="span2" >\
                                            <option data-path="img/mysor.png" value="1">Мусорный бак переполнен</option>\
                                            <option data-path="img/car.png" value="2">Брошенный автомобиль</option>\
                                            <option data-path="img/akk.png" value="3">Пункт приема аккамуляторов</option>\
                                            <option data-path="img/mysor2.png" value="4">Мусор</option>\
                                        </select></div>\
                             </div></div>\
                         <button type="submit" class="btn btn-success">Сохранить</button>\
                         </div>'});

            var myPlacemark = new ymaps.Placemark(coords);

            //Добавляем картинку при выборе опции select
            $('#image').change(function () {
                $('.add-on').find('img:first').attr('src', $('#image option:selected').attr('data-path'));
            });

            //Сохраняем данные из формы
            $('#menu button[type="submit"]').click(function () {
                var iconText = $('input[name="icon_text"]').val(),
                    hintText = $('input[name="hint_text"]').val(),
                    balloonText = $('input[name="balloon_text"]').val(),
                    stylePlacemark = $('select[@name=image] option:selected').text();

                //Добавляем метку на карту
                myMap.geoObjects.add(myPlacemark);


                //Изменяем свойства метки и балуна
                myPlacemark.properties.set({
                    iconContent: iconText,
                    hintContent: hintText,
                    balloonContent: balloonText
                });
                var e = document.getElementById("marker-type-selector");
                var value = e.options[e.selectedIndex].value;
                var imgpath = [
                    'icons/greymysorkared.png',
                    'icons/markergreycar.png',
                    'icons/greyakk.png',
                    'icons/greymysorkagreen.png'
                ];
                myPlacemark.options.set({
                    preset: stylePlacemark,
                    iconImageHref: imgpath[value - 1]
                });
                //Закрываем балун
                myMap.balloon.close();
            });
        } else {
            myMap.balloon.close();
        }
    });
}