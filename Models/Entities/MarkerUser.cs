﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models.Entities
{
    public class MarkerUser
    {
        public int MarkerId { get; set; }
        public virtual Marker Marker { get; set; }


        public string UserId { get; set; }
        public virtual EcoUser EcoUser { get; set; }
    }
}
